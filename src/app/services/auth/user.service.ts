import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { Http, Response, Headers } from "@angular/http";
import { NgForm }   from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import "rxjs/Rx";


@Injectable()
export class UserService {
  private userUrl = environment.apiUrl + 'extra';


private currentUserSource = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
  public currentUser = this.currentUserSource.asObservable();

  public constructor(private router: Router, private http: Http) {}
  private headers = new Headers({'Content-Type': 'application/json'});

  public formErrors={};

  public changeUser(obj: any)
  {
    localStorage.setItem('currentUser', JSON.stringify(obj));
    this.currentUserSource.next(obj);
  }

  //user forgot password
  public forgotPassword(form: NgForm): Promise<any> {
    const url = `${this.userUrl}/forgot-password`;
    //console.log(url);
    return this.http.post(url, JSON.stringify(form.value), {headers: this.headers})
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  //user reset password
  public resetPassword(form: NgForm): Promise<any> {
    const url = `${this.userUrl}/reset-password`;
    //console.log(url);
    return this.http.post(url, JSON.stringify(form.value), {headers: this.headers})
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  //user logout
  public logout(token: string): Promise<any> {
    const url = `${this.userUrl}/logout?token=`+ token;
    //console.log(url);
    return this.http.get(url, {headers: this.headers})
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

// error handling
   private handleError(error: any): Promise<any>
   {
     console.error('An error occurred', error);
     return Promise.reject(error.message || error);
  }

  //return promise data
  private extractData(response: Response) {
    let results = response.json();
    return results || {};
  }

}
