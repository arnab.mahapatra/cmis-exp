import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { Http, Response, Headers } from "@angular/http";
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Injectable()
  export class LoginService {
    private loginUrl = environment.apiUrl + 'extra/login';

    constructor(private router: Router, private http: Http) {}
    private headers = new Headers({'Content-Type': 'application/json'});

  public formErrors={};

  public login(email: string, password: string) {
    const url = `${this.loginUrl}`;
    return this.http.post(url, { email: email,password:password }).map((res: Response) => {
        return res.json();
    });
  }


    // error handling
      private handleError(error: any): Promise<any> {
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
    }

}
