import { Component, ViewChild, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';
import {MatSidenav} from '@angular/material/sidenav';
import { UserService } from '../../../services/auth/user.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {

  // isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  //   .pipe(
  //     map(result => result.matches)
  //   );

  constructor(public router: Router, private breakpointObserver: BreakpointObserver, public us: UserService, private logoutValidator: MatSnackBar) {}
   durationInSeconds = 5;
   public user: string;
   public token: string;
   public userDetails: any;

  @ViewChild('sidenav') sidenav: MatSidenav;

  reason = '';

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  public logout(fm): void {
    this.us.logout(this.token).then(
      resultArray => {
        if (resultArray.status) {
            localStorage.removeItem('currentUser');
            localStorage.clear();
            this.router.navigate(['/']);
            this.logoutValidator.open("You are signed out from your profile.", "ok",{
              duration: this.durationInSeconds * 1000,
              verticalPosition: 'top'
            });
        }
      },
      error => console.log("Error :: " + error)
    )

}

ngOnInit() {
  this.user = localStorage.getItem('currentUser');
  let getUser = JSON.parse(this.user);
  this.token = getUser.token;
  this.userDetails =  getUser.user;
}


}
