import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpModule} from '@angular/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import * as material from '@angular/material';
// import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatProgressBarModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatMenuModule} from '@angular/material/menu';



import { ProfileRoutingModule } from './profile-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { UserService } from '../../services/auth/user.service';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';

@NgModule({
  declarations: [DashboardComponent, MainNavComponent],
  imports: [
    CommonModule,
    HttpModule,
    ProfileRoutingModule,
    material.MatSelectModule,
    material.MatOptionModule,
    material.MatFormFieldModule,
    material.MatInputModule,
    material.MatToolbarModule,
    material.MatButtonModule,
    material.MatSidenavModule,
    material.MatIconModule,
    material.MatListModule,
    material.MatCardModule,
    material.MatGridListModule,
    FormsModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    FlexLayoutModule,
    MatMenuModule
  ],
  providers: [
    UserService,
  ],
})
export class ProfileModule { }
