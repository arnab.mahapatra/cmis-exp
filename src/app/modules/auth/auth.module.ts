import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpModule} from '@angular/http';
import { AuthRoutingModule } from './auth-routing.module';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { LoginComponent } from './components/login/login.component';
import { LoginService } from '../../services/auth/login.service';
import { UserService } from '../../services/auth/user.service';

import * as material from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';

@NgModule({
  declarations: [LoginComponent, ForgotPasswordComponent, ResetPasswordComponent],
  exports: [LoginComponent,
    material.MatSelectModule,
    material.MatOptionModule,
    material.MatFormFieldModule,
    material.MatInputModule,
    material.MatToolbarModule,
    material.MatButtonModule,
    material.MatSidenavModule,
    material.MatIconModule,
    material.MatListModule,
    material.MatCardModule,
    material.MatGridListModule,
     ],
  imports: [
    CommonModule,
    HttpModule,
    AuthRoutingModule,
    MatSnackBarModule,
    material.MatSelectModule,
    material.MatOptionModule,
    material.MatFormFieldModule,
    material.MatInputModule,
    material.MatToolbarModule,
    material.MatButtonModule,
    material.MatSidenavModule,
    material.MatIconModule,
    material.MatListModule,
    material.MatCardModule,
    material.MatGridListModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    LoginService,
    UserService,
  ],
})
export class AuthModule { }
