import { Component, OnInit } from '@angular/core';
import { NgForm }   from '@angular/forms';
import {FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { UserService } from '../../../../services/auth/user.service';
import 'rxjs/add/operator/toPromise';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  constructor(public router: Router, private activeRoute: ActivatedRoute, public us: UserService, private passwordValidator: MatSnackBar) {}
  public formError: string;
  public vcode: string;
  durationInSeconds = 5;
  
  public resetPassword(fm): void {
      if (fm.valid) {
        fm.value.vcode = this.vcode;
        this.us.resetPassword(fm)
        .then(resultArray => {
          if (resultArray.status) {
            this.passwordValidator.open("Password Change Sucessfully", "ok",{
              duration: this.durationInSeconds * 1000,
              verticalPosition: 'top'
            });
            this.router.navigate(['/']);
          }
          else {
            if (typeof resultArray.message == 'string') {
              this.formError = resultArray.message;
            }
            else {
              this.formError = JSON.parse(resultArray.message);
            }
            this.passwordValidator.open(this.formError, "ok",{
              duration: this.durationInSeconds * 1000,
              verticalPosition: 'top'
            });
          } 
        })
        .catch(error => console.log("Error :: " + error));
      }
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      this.vcode = params['vcode'];
      //console.log(this.vcode);

      if (!this.vcode) {
        this.passwordValidator.open("Unauthorised access.", "ok",{
          duration: this.durationInSeconds * 1000,
          verticalPosition: 'top'
        });
        this.router.navigate(['/']);
      }
    });
  }

}
