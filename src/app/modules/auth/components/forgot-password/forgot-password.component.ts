import { Component, OnInit } from '@angular/core';
import { NgForm }   from '@angular/forms';
import {FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { UserService } from '../../../../services/auth/user.service';
import 'rxjs/add/operator/toPromise';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(public router: Router, private activeRoute: ActivatedRoute, public us: UserService, private passwordValidator: MatSnackBar) { }
  public formError: string;
  durationInSeconds = 5;

  public forgotPassword(fm): void {
      if (fm.valid) {
        this.us.forgotPassword(fm)
        .then(resultArray => {
          if (resultArray.status) {
            this.formError = resultArray.message;
            this.passwordValidator.open(this.formError, "ok",{
              duration: this.durationInSeconds * 1000,
              verticalPosition: 'top'
            });
          }
          else {
            if (typeof resultArray.message == 'string') {
              this.formError = resultArray.message;
            }
            else {
              this.formError = JSON.parse(resultArray.message);
            }
            this.passwordValidator.open(this.formError, "ok",{
              duration: this.durationInSeconds * 1000,
              verticalPosition: 'top'
            });
          }
        })
        .catch(error => console.log("Error :: " + error));
      }
      else {
        console.log('Error: Form not validate!');
      }
  }

  ngOnInit() {
  }

}
