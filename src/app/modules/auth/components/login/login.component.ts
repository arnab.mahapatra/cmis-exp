import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { LoginService } from '../../../../services/auth/login.service';
import { UserService } from '../../../../services/auth/user.service';
import 'rxjs/add/operator/toPromise';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formErrors: string;
  public form = {
    email : null,
    password : null
  };
  hide = true;
  durationInSeconds = 5;

  constructor(public router: Router, private activeRoute: ActivatedRoute, public ls: LoginService, public us: UserService, private loginValidator: MatSnackBar) { }

   loginSubmit(loginFormData) {
    if(loginFormData.valid) {
      this.ls.login(loginFormData.value.email, loginFormData.value.password).subscribe(res => {
        if(res.status == true) {
          this.us.changeUser(res.data);
          this.router.navigate(['/profile/dashboard']);
        }
        else
        {
          this.formErrors = res.message;
          this.loginValidator.open(this.formErrors, "ok",{
            duration: this.durationInSeconds * 1000,
            verticalPosition: 'top'
          });
        }
      });
    }
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser') != 'undefined') {
      this.router.navigate(['/profile/dashboard']);
    }
  }

}
