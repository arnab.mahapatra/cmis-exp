import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { from } from 'rxjs';

const routes: Routes = [
  {
    path:"",
    loadChildren: './modules/auth/auth.module#AuthModule',
  },
  {
    path:"forgot-password",
    loadChildren: './modules/auth/auth.module#AuthModule',
  },
  {
    path:"profile",
    loadChildren: './modules/profile/profile.module#ProfileModule',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
